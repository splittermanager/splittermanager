package de.splittermond.generator.ui.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;


/**
 * Created by jones on 5/16/16.
 */
@Controller
public class UserControl {

    @Autowired
    WebSecurityConfig securityConfig;

    @RequestMapping(value = "/createUser", method = RequestMethod.PUT)
    public String createUser(String username, String password) {
        try {
            securityConfig.addUser(username, password);
        } catch (IllegalArgumentException e) {
            return "redirect:login?duplicate";
        }
        securityConfig.loginUser(username);

        return "redirect:home";
    }

    @RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }

}
