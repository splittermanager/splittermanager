package de.splittermond.generator.ui.model;

import lombok.Getter;

/**
 * Created by jones on 6/28/16.
 */
public enum DerivedValue implements HeroEnum {

    GK("#{Groessenklasse}", ""),
    GSW("#{Geschwindigkeit}", "derivedValues[GK].skilled + attributes[BEW].skilled"),
    INI("#{Initiative}", "10 - attributes[INT].skilled"),
    LP("#{Lebenspunkte}", "derivedValues[GK].skilled + attributes[KON].skilled"),
    FO("#{Fokus}", "2 * (attributes[MYS].skilled + attributes[WIL].skilled)"),
    VTD("#{Verteidigung}", "12 + attributes[BEW].skilled + attributes[STA].skilled"),
    GW("#{Geistiger Widerstand}", "12 + attributes[VER].skilled + attributes[WIL].skilled"),
    KW("{Koerperl. Widerstand}", "12 + attributes[KON].skilled + attributes[WIL].skilled");

    @Getter
    private final String name;
    @Getter
    private final String rule;

    private DerivedValue(String name, String rule) {
        this.name = name;
        this.rule = rule;
    }

}
