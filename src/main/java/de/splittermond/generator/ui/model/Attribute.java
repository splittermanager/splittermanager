package de.splittermond.generator.ui.model;

import lombok.Getter;

/**
 * Created by jones on 6/19/16.
 */
public enum Attribute implements HeroEnum {
    AUS("#{AUS}"),
    BEW("#{BEW}"),
    INT("#{INT}"),
    KON("#{KON}"),
    MYS("#{MYS}"),
    STA("#{STA}"),
    VER("#{VER}"),
    WIL("#{WIL}");

    @Getter
    private final String name;

    private Attribute(String name) {
        this.name = name;
    }

}
