package de.splittermond.generator.ui.model;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by jones on 7/6/16.
 */
@Component
public class HeroContext {
    @Getter
    private Map<String, Skill> skills = new HashMap<>();
    @Getter
    private Map<String, Attribute> attributes = new HashMap<>();
    @Getter
    private Map<String, DerivedValue> derivedValues = new HashMap<>();

    public HeroContext() {
        skills.putAll(Arrays.stream(Skill.values())
                .collect(Collectors.toMap(s -> s.name(), s -> s)));
        attributes.putAll(Arrays.stream(Attribute.values())
                .collect(Collectors.toMap(a -> a.name(), a -> a)));
        derivedValues.putAll(Arrays.stream(DerivedValue.values())
                .collect(Collectors.toMap(d -> d.name(), d -> d)));
    }
}
