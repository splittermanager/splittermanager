package de.splittermond.generator.ui.model;

import lombok.Getter;

import static de.splittermond.generator.ui.model.Attribute.*;

/**
 * Created by jones on 6/19/16.
 */
public enum Skill implements HeroEnum {
    Akrobatik("#{Akrobatik}", BEW, STA, AttributeType.DEFAULT),
    Alchemie("#{Alchemie}", MYS, VER, AttributeType.DEFAULT),
    Anfuehren("#{Anfuehren}", AUS, WIL, AttributeType.DEFAULT),
    ArkaneKunde("#{ArkaneKunde}", MYS, VER, AttributeType.DEFAULT),
    Athletik("#{Athletik}", BEW, STA, AttributeType.DEFAULT),
    Darbietung("#{Darbietung}", AUS, WIL, AttributeType.DEFAULT),
    Diplomatie("#{Diplomatie}", AUS, VER, AttributeType.DEFAULT),
    Edelhandwerk("#{Edelhandwerk}", INT, VER, AttributeType.DEFAULT),
    Empathie("#{Empathie}", INT, VER, AttributeType.DEFAULT),
    Entschlossenheit("#{Entschlossenheit}", AUS, WIL, AttributeType.DEFAULT),
    Fingerfertigkeit("#{Fingerfertigkeit}", AUS, BEW, AttributeType.DEFAULT),
    GeschichteMythen("#{GeschichteMythen}", MYS, VER, AttributeType.DEFAULT),
    Handwerk("#{Handwerk}", KON, VER, AttributeType.DEFAULT),
    Heilkunde("#{Heilkunde}", INT, VER, AttributeType.DEFAULT),
    Heimlichkeit("#{Heimlichkeit}", BEW, INT, AttributeType.DEFAULT),
    Jagdkunst("#{Jagdkunst}", KON, VER, AttributeType.DEFAULT),
    Laenderkunde("#{Laenderkunde}", INT, VER, AttributeType.DEFAULT),
    Naturkunde("#{Naturkunde}", INT, VER, AttributeType.DEFAULT),
    Redegewandtheit("#{Redegewandtheit}", AUS, WIL, AttributeType.DEFAULT),
    SchloesserFallen("#{SchloesserFallen}", INT, BEW, AttributeType.DEFAULT),
    Schwimmen("#{Schwimmen}", STA, KON, AttributeType.DEFAULT),
    Seefahrt("#{Seefahrt}", BEW, KON, AttributeType.DEFAULT),
    Straßenkunde("#{Straßenkunde}", AUS, INT, AttributeType.DEFAULT),
    Tierfuehrung("#{Tierfuehrung}", AUS, BEW, AttributeType.DEFAULT),
    Ueberleben("#{Ueberleben}", INT, KON, AttributeType.DEFAULT),
    Wahrnehmung("#{Wahrnehmung}", INT, WIL, AttributeType.DEFAULT),
    Zaehigkeit("#{Zaehigkeit}", KON, WIL, AttributeType.DEFAULT),

    Bann("#{Bann}", MYS, WIL, AttributeType.MAGIC),
    Beherrschung("#{Beherrschung}", MYS, WIL, AttributeType.MAGIC),
    Bewegung("#{Bewegung}", MYS, BEW, AttributeType.MAGIC),
    Erkenntnis("#{Erkenntnis}", MYS, VER, AttributeType.MAGIC),
    Fels("#{Fels}", MYS, KON, AttributeType.MAGIC),
    Feuer("#{Feuer}", MYS, AUS, AttributeType.MAGIC),
    Heilung("#{Heilung}", MYS, AUS, AttributeType.MAGIC),
    Illusion("#{Illusion}", MYS, AUS, AttributeType.MAGIC),
    Kampf("#{Kampf}", MYS, STA, AttributeType.MAGIC),
    Licht("#{Licht}", MYS, AUS, AttributeType.MAGIC),
    Natur("#{Natur}", MYS, AUS, AttributeType.MAGIC),
    Schatten("#{Schatten}", MYS, INT, AttributeType.MAGIC),
    Schicksal("#{Schicksal}", MYS, AUS, AttributeType.MAGIC),
    Schutz("#{Schutz}", MYS, AUS, AttributeType.MAGIC),
    Staerkung("#{Staerkung}", MYS, STA, AttributeType.MAGIC),
    Tod("#{Tod}", MYS, VER, AttributeType.MAGIC),
    Verwandlung("#{Verwandlung}", MYS, KON, AttributeType.MAGIC),
    Wasser("#{Wasser}", MYS, INT, AttributeType.MAGIC),
    Wind("#{Wind}", MYS, VER, AttributeType.MAGIC),

    Handgemenge("{#Handgemenge}", null, null, AttributeType.FIGHT),
    Hiebwaffen("{#Hiebwaffen}", null, null, AttributeType.FIGHT),
    Kettenwaffen("{#Kettenwaffen}", null, null, AttributeType.FIGHT),
    Klingenwaffen("{#Klingenwaffen}", null, null, AttributeType.FIGHT),
    Stangenwaffen("{#Stangenwaffen}", null, null, AttributeType.FIGHT),
    Schusswaffen("{#Schusswaffen}", null, null, AttributeType.FIGHT),
    Wurfwaffen("{#Wurfwaffen}", null, null, AttributeType.FIGHT),
    Waffenlos("{#Waffenlos}", BEW, STA, AttributeType.FIGHT);


    @Getter
    private final String name;
    @Getter
    private final Attribute a;
    @Getter
    private final Attribute b;
    @Getter
    private final AttributeType type;

    private Skill(String name, Attribute a, Attribute b, AttributeType type) {
        this.name = name;
        this.a = a;
        this.b = b;
        this.type = type;
    }

}
