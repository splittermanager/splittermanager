package de.splittermond.generator.ui.model;

import com.google.common.collect.ImmutableMap;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by jones on 6/20/16.
 */
public class Hero {
    @Getter
    @Setter
    private long id;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private int xp;
    @Getter
    @Setter
    private int level;
    @Getter
    @Setter
    private String culture;
    @Getter
    @Setter
    private String race;
    @Getter
    @Setter
    private String haircolor;
    @Getter
    @Setter
    private String eyecolor;
    @Getter
    @Setter
    private String skincolor;
    @Getter
    @Setter
    private String origin;
    @Getter
    @Setter
    private float height;
    @Getter
    @Setter
    private String birthlocation;
    @Getter
    @Setter
    private String moonsign;

    @Getter
    private Map<Attribute, Value> attributes;
    @Getter
    private Map<Skill, Value> skills;
    @Getter
    private Map<DerivedValue, Value> derivedValues;
    @Getter
    @Setter
    private Set<Mastery> masteries;
    @Getter
    @Setter
    private Set<SpecialFeature> weaknesses;
    @Getter
    @Setter
    private Set<SpecialFeature> strength;

    public Hero() {
        attributes = ImmutableMap.copyOf(
                Arrays.stream(Attribute.values())
                        .collect(Collectors.toMap(at -> at, at -> new Value())));
        skills = ImmutableMap.copyOf(
                Arrays.stream(Skill.values())
                        .collect(Collectors.toMap(s -> s, s -> new Value())));
        derivedValues = ImmutableMap.copyOf(
                Arrays.stream(DerivedValue.values())
                        .collect(Collectors.toMap(d -> d, d -> new Value())));
    }
}
