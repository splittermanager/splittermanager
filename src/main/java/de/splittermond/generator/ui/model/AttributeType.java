package de.splittermond.generator.ui.model;

/**
 * Created by jones on 6/25/16.
 */
public enum AttributeType {
    DEFAULT, FIGHT, MAGIC
}
