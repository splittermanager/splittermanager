package de.splittermond.generator.ui.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jones on 6/25/16.
 */
public class Mastery extends SpecialFeature{
    @Getter
    @Setter
    Skill skill;

}
