package de.splittermond.generator.ui.model;

/**
 * Created by jones on 6/28/16.
 */
public enum SpecialFeatureType {
    MASTERY, STRENGTH, WEAKNESS
}
