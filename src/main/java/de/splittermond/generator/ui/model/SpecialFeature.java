package de.splittermond.generator.ui.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by jones on 6/25/16.
 */
public class SpecialFeature {

    @Getter
    @Setter
    long id;
    @Getter
    @Setter
    String name;
    @Getter
    @Setter
    int level;
    @Getter
    @Setter
    String Description;
}
