package de.splittermond.generator.ui.control;

import de.splittermond.generator.ui.service.HeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by jones on 5/23/16.
 */
@Controller
public class HeroControl {

    @Autowired
    private HeroService heroService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String getHeroes(Model model) {
        model.addAttribute("heroes", heroService.getOwnHeroes());
        return "home";
    }

    @RequestMapping(value = "/createHero", method = RequestMethod.PUT)
    public String createHero(@RequestParam("heroName") String name) {
        heroService.addHero(name);
        return "redirect:home";
    }
}
