package de.splittermond.generator.ui.service;

import de.splittermond.generator.ui.jpa.entity.IdentifiableEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.jdbc.JdbcMutableAclService;
import org.springframework.security.acls.model.MutableAcl;
import org.springframework.security.acls.model.ObjectIdentity;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.acls.model.Sid;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * Created by jones on 6/9/16.
 */
@Service
public class PermissionService {
    @Autowired
    JdbcMutableAclService aclService;

    public void createEntry(IdentifiableEntity idEntity, Permission p) {
        // Prepare the information we'd like in our access control entry (ACE)
        ObjectIdentity oi = new ObjectIdentityImpl(idEntity);

        // Create or update the relevant ACL
        MutableAcl acl = aclService.createAcl(oi);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Sid sid = new PrincipalSid(auth);
        // Now grant some permissions via an access control entry (ACE)
        acl.insertAce(acl.getEntries().size(), p, sid, true);
        aclService.updateAcl(acl);
    }
}
