package de.splittermond.generator.ui.service;

import de.splittermond.generator.ui.jpa.entity.HeroEntity;
import de.splittermond.generator.ui.jpa.entity.ValueEntity;
import de.splittermond.generator.ui.model.Hero;
import de.splittermond.generator.ui.model.HeroContext;
import de.splittermond.generator.ui.model.HeroEnum;
import de.splittermond.generator.ui.model.Value;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * Created by jones on 6/20/16.
 */
@Service
public class HeroConverterService {

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    HeroContext heroContext;

    HeroEntity convertToEntity(Hero dto) {
        HeroEntity entity = new HeroEntity();
        modelMapper.map(dto, entity);
        return entity;
    }

    Hero convertToDTO(HeroEntity entity) {
        Hero dto = new Hero();
        modelMapper.map(entity, dto);
        fillTransientFields(dto, entity);
        return dto;
    }

    private void fillTransientFields(Hero dto, HeroEntity entity) {
        dto.setLevel(calculateLevel(1, 100, dto.getXp()));

        entity.getHeroModifications()
                .forEach(m -> m.getHeroAttributes()
                        .forEach(at -> addValue(at, dto.getAttributes())));

        entity.getHeroModifications()
                .forEach(m -> m.getHeroSkills()
                        .forEach(s -> addValue(s, dto.getSkills())));

        dto.getDerivedValues().entrySet()
                .forEach(e -> e.getValue().computed = eval(e.getKey().getRule(), dto));


        entity.getHeroModifications()
                .forEach(m -> m.getHeroSpecialFeatures()
                        .forEach(sf -> evalAssignment(sf.getRule(), dto)));
    }

    private void evalAssignment(String rule, Hero dto) {
        if (StringUtils.isEmpty(rule)) {
            return;
        }

        ExpressionParser parser = new SpelExpressionParser();
        EvaluationContext context = new StandardEvaluationContext(heroContext);
        Expression exp = parser.parseExpression(rule.split("=")[0]);
        HeroEnum assignee = exp.getValue(context, HeroEnum.class);

        context = new StandardEvaluationContext(dto);
        exp = parser.parseExpression(rule.split("=")[1]);
        Integer value = exp.getValue(context, Integer.class);

        addValue(assignee, value, dto);
    }

    private void addValue(HeroEnum assignee, int value, Hero dto) {
        Value val = dto.getDerivedValues().get(assignee);
        if (val != null) {
            val.computed += value;
            return;
        }
        val = dto.getSkills().get(assignee);
        if (val != null) {
            val.computed += value;
            return;
        }
        val = dto.getAttributes().get(assignee);
        if (val != null) {
            val.computed += value;
            return;
        }
        throw new IllegalArgumentException("No matching Enum for " + assignee.toString());
    }

    private int eval(String rule, Hero dto) {
        if (StringUtils.isEmpty(rule)) return 0;
        ExpressionParser parser = new SpelExpressionParser();
        Expression exp = parser.parseExpression(rule);
        EvaluationContext context = new StandardEvaluationContext(dto);
        return (Integer) exp.getValue(context);

    }

    private void addValue(ValueEntity at, Map<? extends HeroEnum, Value> values) {
        Value value = values.get(at.getEnum());
        value.skilled += at.getValue();
        value.computed += at.getValue();
    }

    private int calculateLevel(int level, int upperLimit, int xp) {
        if (xp - upperLimit > 0) {
            return calculateLevel(++level, level * 100 + upperLimit, xp);
        }
        return level;
    }

}
