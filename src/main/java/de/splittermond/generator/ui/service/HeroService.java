package de.splittermond.generator.ui.service;

import de.splittermond.generator.ui.jpa.entity.HeroEntity;
import de.splittermond.generator.ui.jpa.entity.HeroModification;
import de.splittermond.generator.ui.jpa.repository.HeroRepo;
import de.splittermond.generator.ui.model.Hero;
import de.splittermond.generator.ui.model.ModificationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jones on 6/9/16.
 */
@Service
@EnableTransactionManagement
public class HeroService {
    @Autowired
    HeroRepo heroRepo;

    @Autowired
    PermissionService permissionService;

    @Autowired
    HeroConverterService converterService;

    public List<Hero> getOwnHeroes() {
        return getHeroes().stream()
                .map(e -> converterService.convertToDTO(e))
                .collect(Collectors.toList());
    }

    @PostFilter("hasPermission(filterObject, 'ADMINISTRATION')")
    private List<HeroEntity> getHeroes() {
        return heroRepo.findAll();
    }

    @Transactional
    public void addHero(String name) {
        HeroEntity heroEntity = new HeroEntity(name);
        HeroModification mo = new HeroModification();
        mo.setType(ModificationType.CUSTOM);
        heroEntity.setHeroModifications(Collections.singleton(mo));
        HeroEntity hero = heroRepo.save(heroEntity);
        permissionService.createEntry(hero, BasePermission.ADMINISTRATION);
    }
}
