package de.splittermond.generator.ui.jpa.entity;

import de.splittermond.generator.ui.model.ModificationType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by jones on 6/25/16.
 */
@Entity
public class HeroModification {

    @Column
    @GeneratedValue
    @Id
    @Getter
    @Setter
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @Getter
    @Setter
    private ModificationType type;

    @Column
    @Getter
    @Setter
    private String name;

    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE}, orphanRemoval = true)
    @JoinColumn(name = "hero_modification_id", referencedColumnName = "id")
    @Getter
    @Setter
    private Set<HeroAttribute> heroAttributes;

    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE}, orphanRemoval = true)
    @JoinColumn(name = "hero_modification_id", referencedColumnName = "id")
    @Getter
    @Setter
    private Set<HeroSkill> heroSkills;

    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE}, orphanRemoval = true)
    @JoinColumn(name = "hero_modification_id", referencedColumnName = "id")
    @Getter
    @Setter
    private Set<HeroSpecialFeature> heroSpecialFeatures;
}
