package de.splittermond.generator.ui.jpa.entity;

import de.splittermond.generator.ui.model.Skill;
import de.splittermond.generator.ui.model.SpecialFeatureType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by jones on 6/25/16.
 */
@Entity
public class HeroSpecialFeature {
    @Column
    @GeneratedValue
    @Id
    @Getter
    @Setter
    private long id;

    @Column(nullable = false)
    @Getter
    @Setter
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @Getter
    @Setter
    private Skill skill;

    @Column(nullable = false)
    @Getter
    @Setter
    private int level;

    @Column
    @Getter
    @Setter
    private String description;

    @Enumerated(EnumType.STRING)
    @Column
    @Getter
    @Setter
    private SpecialFeatureType type;

    @Column
    @Getter
    @Setter
    private String rule;

}
