package de.splittermond.generator.ui.jpa.entity;

import de.splittermond.generator.ui.model.HeroEnum;

/**
 * Created by jones on 7/4/16.
 */
public interface ValueEntity {
    public int getValue();
    public HeroEnum getEnum();
}
