package de.splittermond.generator.ui.jpa.entity;

import de.splittermond.generator.ui.model.HeroEnum;
import de.splittermond.generator.ui.model.Skill;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by jones on 5/23/16.
 */
@Entity
public class HeroSkill implements ValueEntity{
    @Column
    @GeneratedValue
    @Id
    @Getter
    @Setter
    private long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @Getter
    @Setter
    private Skill skill;

    @Column(nullable = false)
    @Getter
    @Setter
    private int value;


    @Override
    public HeroEnum getEnum() {
        return skill;
    }
}
