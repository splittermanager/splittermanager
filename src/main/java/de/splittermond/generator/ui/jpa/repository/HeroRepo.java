package de.splittermond.generator.ui.jpa.repository;

import de.splittermond.generator.ui.jpa.entity.HeroEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by jones on 5/23/16.
 */
@Repository
public interface HeroRepo extends JpaRepository<HeroEntity, Integer> {
    public HeroEntity findById(@Param("id") Long id);
}
