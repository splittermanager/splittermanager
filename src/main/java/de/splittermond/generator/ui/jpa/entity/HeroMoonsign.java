package de.splittermond.generator.ui.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by jones on 6/28/16.
 */
@Entity
public class HeroMoonsign {

    @Column(nullable = false)
    @Id
    @Getter
    @Setter
    private String name;

    @Column
    @Getter
    @Setter
    private String description;
}
