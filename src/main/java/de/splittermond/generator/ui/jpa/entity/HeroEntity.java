package de.splittermond.generator.ui.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by jones on 5/23/16.
 */
@Entity(name = "hero")
public class HeroEntity implements IdentifiableEntity {
    @Column
    @GeneratedValue
    @Id
    @Getter
    @Setter
    private long id;

    @Column(nullable = false)
    @Getter
    @Setter
    private String name;

    @Column
    @Getter
    @Setter
    private int xp;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinTable(name = "hero_hero_modification",
            joinColumns = {@JoinColumn(name = "hero_id", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "hero_modification_id", nullable = false, updatable = false)})
    @Getter
    @Setter
    private Set<HeroModification> heroModifications;

    private String haircolor;
    @Column
    @Getter
    @Setter
    private String eyecolor;
    @Column
    @Getter
    @Setter
    private String skincolor;
    @Column
    @Getter
    @Setter
    private float height;
    @Column
    @Getter
    @Setter
    private String birthlocation;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(referencedColumnName = "name", name = "hero_moonsign_name")
    @Getter
    @Setter
    private HeroMoonsign moonsign;

    public HeroEntity() {
    }

    public HeroEntity(String name) {
        setName(name);
    }
}
