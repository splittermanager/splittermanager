package de.splittermond.generator.ui.jpa.entity;

/**
 * Created by jones on 6/9/16.
 */
public interface IdentifiableEntity {
    public long getId();
}
