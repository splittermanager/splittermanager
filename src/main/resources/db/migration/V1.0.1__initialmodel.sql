create table hero_moonsign(
name varchar(255) not null primary key,
description varchar (4096)
);

create table hero(
id bigint not null primary key auto_increment,
name varchar(350) not null,
xp int,
hero_attributes_id bigint,
haircolor varchar (255),
eyecolor varchar (255),
skincolor varchar (255),
height float,
birthlocation varchar (255),
hero_moonsign_name varchar(255),
foreign key (hero_moonsign_name) references hero_moonsign(name)
);

create table hero_modification(
id bigint not null primary key auto_increment,
type varchar(255) not null,
name varchar(255)
);

create table hero_hero_modification(
hero_id bigint not null,
hero_modification_id bigint not null,
primary key (hero_id, hero_modification_id),
foreign key (hero_modification_id) references hero_modification(id),
foreign key (hero_id) references hero(id)
);

create table hero_attribute(
id bigint not null primary key auto_increment,
hero_modification_id bigint not null,
attribute varchar(255) not null,
`value` int not null,
foreign key (hero_modification_id) references hero_modification(id),
unique index hero_attribute_idx_1(hero_modification_id, attribute)
);

create table hero_special_feature(
id bigint not null primary key auto_increment,
hero_modification_id bigint not null,
name varchar(255) not null,
level int not null,
description varchar(4096),
type varchar(45),
rule varchar (444),
foreign key (hero_modification_id) references hero_modification(id)
);

create table hero_skill(
id bigint not null primary key auto_increment,
hero_modification_id bigint not null,
skill varchar(255) not null,
`value` int not null,
foreign key (hero_modification_id) references hero_modification(id)
);
