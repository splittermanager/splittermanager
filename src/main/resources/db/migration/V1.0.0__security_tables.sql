create table users (
username varchar(50) not null primary key,
password varchar(255) not null,
enabled boolean not null);

create table authorities (
username varchar(50) not null,
authority varchar(50) not null,
foreign key (username) references users (username),
unique index authorities_idx_1 (username, authority));

create table acl_sid (
  id bigint not null primary key auto_increment,
  principal boolean not null,
  sid varchar(100) not null,
  unique index unique_index_acl_sid (sid,principal) );

create table acl_class (
  id bigint not null primary key auto_increment,
  class varchar(100) not null,
  unique index unique_index_acl_class (class) );

create table acl_object_identity (
  id bigint not null primary key auto_increment,
  object_id_class bigint not null,
  object_id_identity bigint not null,
  parent_object bigint,
  owner_sid bigint not null,
  entries_inheriting boolean not null,
  unique index unique_index_acls_object_identity(object_id_class,object_id_identity),
  foreign key(parent_object)references acl_object_identity(id),
  foreign key(object_id_class)references acl_class(id),
  foreign key(owner_sid)references acl_sid(id) );

create table acl_entry (
  id bigint not null primary key auto_increment,
  acl_object_identity bigint not null,ace_order int not null,sid bigint not null,
  mask integer not null,granting boolean not null,audit_success boolean not null,
  audit_failure boolean not null,
  unique index unique_index_acl_entry (acl_object_identity,ace_order),
  foreign key(acl_object_identity) references acl_object_identity(id),
  foreign key(sid) references acl_sid(id) );
